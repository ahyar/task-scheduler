package com.task.scheduler.scheduler;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RunnableTask implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(RunnableTask.class);
    private String message;
    private String source;

    public RunnableTask(String source, String message) {
        this.message = message;
        this.source = source;
    }

    @Override
    public void run() {
        logger.info("[{}] messages : {}", source, message);
    }
}
