package com.task.scheduler.service;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Service;

import com.task.scheduler.scheduler.RunnableTask;

import org.springframework.scheduling.support.CronTrigger;

@Service
public class TaskSchService {
    private static final Logger logger = LoggerFactory.getLogger(TaskSchService.class);

    // final String source;
    // final String cronSample;
    // final String sampleMessages;

    // public TaskSchService(String source, String croneSample, String
    // sampleMessages) {
    // this.source = source;
    // this.cronSample = croneSample;
    // this.sampleMessages = sampleMessages;
    // }

    @Autowired
    private TaskScheduler taskScheduler;

    private final Map<String, ScheduledFuture<?>> listSch = new HashMap<>();

    // @Override
    public void run(String source, String cronSample, String sampleMessages) {

        logger.info("starting sample sch for {} with message {} in {}", source, sampleMessages, cronSample);
        ScheduledFuture<?> scheduledFuture = taskScheduler.schedule(new RunnableTask(source, sampleMessages),
                new CronTrigger(cronSample));
        listSch.put(source, scheduledFuture);

    }

    public void stop() {

    }
}
