package com.task.scheduler.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.task.scheduler.service.TaskSchService;

@RestController
public class TaskSchController {

    @Autowired
    TaskSchService taskSchService;

    @GetMapping(value = "/start")
    public String start(String source, String cronSample, String sampleMessages) {
        taskSchService.run(source, cronSample, sampleMessages);
        return "STARTED";
    }

    @GetMapping(value = "/stop")
    public String stop() {

        return "STOPPED";
    }

}
