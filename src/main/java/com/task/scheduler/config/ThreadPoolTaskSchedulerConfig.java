package com.task.scheduler.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

@Configuration
public class ThreadPoolTaskSchedulerConfig {

    @Bean
    public TaskScheduler taskScheduler() {

        // threadPoolTaskScheduler.getScheduledThreadPoolExecutor().shutdown();
        return new ThreadPoolTaskScheduler();
    }

}
